package task.enums;

public enum Transparency {
    OPAQUE, TRANSLUCENT, TRANSPARENT
}

package task.view;

import task.controller.GemStoneController;
import task.enums.GemType;
import task.enums.Transparency;
import task.model.GemStone;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Menu {

    private static Scanner scanner = new Scanner(System.in);
    private static GemStoneController gemStoneController = new GemStoneController();
    private static List<GemStone> basket = new ArrayList<>();
    List<GemStone> gems = Arrays.asList(
            new GemStone("Diamond", 17_000.0, 1.0, Transparency.TRANSPARENT, GemType.DIAMOND),
            new GemStone("Emerald", 3_000.0, 1.0, Transparency.TRANSPARENT, GemType.EMERALD),
            new GemStone("Onyx", 600.0, 1.0, Transparency.OPAQUE, GemType.ONYX),
            new GemStone("Amethyst", 25.0, 1.0, Transparency.TRANSLUCENT, GemType.AMETHYST),
            new GemStone("Ruby", 3_500.0, 1.0, Transparency.TRANSPARENT, GemType.RUBY),
            new GemStone("Sapphire", 4_000.0, 1.0, Transparency.TRANSPARENT, GemType.SAPPHIRE),
            new GemStone("Citrine", 300.0, 1.0, Transparency.TRANSPARENT, GemType.CITRINE),
            new GemStone("Spinel", 75.0, 1.0, Transparency.TRANSLUCENT, GemType.SPINEL),
            new GemStone("Quartz", 80.0, 1.0, Transparency.TRANSPARENT, GemType.QUARTZ));

    private void getMenu() {
        System.out.println(" 0-Quit \n"
                + " 1-Check available stones \n "
                + "2-Filter stones by min price and max price  \n "
                + "3-Sort stones by price \n "
                + "4-Add the stone to the basket, and after insert the name of the stone \n "
                + "5-Remove the stone from the basket, and after insert the name of the stone \n "
                + "6-Check the basket and check the total price and weight of the stones \n");
        System.out.println(" \n ");
    }

    public void run() {
        gemStoneController.setGemStones(gems);
        String button;
        do {
            System.out.println(" \n ");
            getMenu();
            button = scanner.nextLine();
            switch (button) {
                case "0":
                    scanner.close();
                    break;
                case "1":
                    gemStoneController.gemStonesDisplay();
                    break;
                case "2":
                    gemStoneController.sortStonesByPrice();
                    System.out.println("Input Minimum Price");
                    int min = scanner.nextInt();
                    System.out.println("Input Maximum Price");
                    int max = scanner.nextInt();
                    gemStoneController.showStones(gemStoneController.filterByPrice(min, max));
                    break;
                case "3":
                    gemStoneController.sortStonesByPrice();
                    gemStoneController.gemStonesDisplay();
                    break;
                case "4":
                    System.out.println("Insert the name of the stone to add to the basket");
                    String nameToAdd = scanner.nextLine();
                    GemStone addToBasket = gemStoneController.getByName(nameToAdd);
                    System.out.println(addToBasket);
                    basket.add(addToBasket);
                    System.out.println("\n" + "You added the stone to the basket");
                    break;
                case "5":
                    System.out.println("Insert the name of the stone to remove from the basket");
                    String nameToDelete = scanner.nextLine();
                    GemStone removeFromBasket = gemStoneController.getByName(nameToDelete);
                    System.out.println(removeFromBasket);
                    basket.remove(removeFromBasket);
                    System.out.println("\n" + "You removed the stone from the basket");
                    break;
                case "6":
                    double price = 0.0;
                    double weight = 0.0;
                    for (GemStone temp : basket) {
                        System.out.println(temp);
                    }
                    for (int i = 0; i < basket.size(); i++) {
                        price += basket.get(i).getPrice();
                    }
                    for (int i = 0; i < basket.size(); i++) {
                        weight += basket.get(i).getWeight();
                    }
                    System.out.println("\n");
                    System.out.println("Total price of the stones in the basket " + price + "\n");
                    System.out.println("Total weight of the stones in the basket " + weight);
                    break;
                default:
                    System.err.println("Wrong number, please try again.");
            }
        }
        while (!button.equals("0"));
        scanner.close();
    }
}

